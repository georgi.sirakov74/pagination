import {Component, Input, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LoginRequest } from "../payload/LoginRequest";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  loginForm: FormGroup;
  user: LoginRequest = new LoginRequest();
  isLogged: boolean = false;
  submitted = false;
  @Input() username: string;
  constructor(
    private formBuilder: FormBuilder
  ) {

  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  loginUser() {
    this.user = this.loginForm.value;
    this.submitted = true;
  }

  login() {
    console.log(this.username);
  }

}
